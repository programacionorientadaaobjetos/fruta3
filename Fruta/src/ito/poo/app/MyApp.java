package ito.poo.app;
import ito.poo.clases.Fruta;
import ito.poo.clases.Periodo;

public class MyApp {
	static void run() {
		Fruta F = new Fruta("Manzana", 20.0f, 200f, 400f);
		Periodo p = new Periodo("septiembre", 400f);
		
		System.out.println(F);
		
		//Agregar
		F.AgregarPeriodo(new Periodo("septiembre", 700f));
		F.AgregarPeriodo(new Periodo("octubre", 600f));
		
		System.out.println(F);
		
		//Eliminar
		System.out.println(F.EliminarPeriodo(1));
		
		System.out.println(F);
		
		//Costo por periodo
		System.out.println(p.costoPeriodo(0, F.getCostoPromedio()));
		
		//Ganancia por periodo
		System.out.println(p.gananciaEstimada(0, F.getPrecioVentaProm()));
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
	}

}
